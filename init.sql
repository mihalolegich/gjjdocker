create table account
(
	id bigserial not null
		constraint accounts_pkey
			primary key,
	name varchar(50),
	lastname varchar(50),
	patronymic varchar(50),
	bday date,
	homeaddress varchar(255),
	workaddress varchar(255),
	email varchar(255) not null
		constraint accounts_email_key
			unique,
	icq integer,
	skype varchar(255),
	addinfo varchar(255),
	pass varchar(255),
	regdate timestamp default CURRENT_TIMESTAMP not null
);

create table friendshiprequest
(
	fromaccid integer,
	toaccid integer,
	constraint friendship_req_fromid_toid_key
		unique (fromaccid, toaccid)
);

create table friendship
(
	accid1 integer,
	accid2 integer,
	constraint friends_id1_id2_key
		unique (accid1, accid2),
	constraint friends_check
		check (accid1 < accid2)
);

create table groupmember
(
	account_id integer,
	group_id integer,
	accisadmin boolean,
	constraint groups_members_accid_groupid_key
		unique (account_id, group_id)
);

create table phone
(
	accid integer
		constraint accid_fk
			references account,
	number varchar(15) not null
		constraint phones_number_key
			unique,
	type varchar(15),
	phoneid bigint not null
		constraint phones_pkey
			primary key
);

create table accountpic
(
	accid integer not null
		constraint profile_pics_pkey
			primary key,
	pic bytea
);

create table principal
(
	accid integer not null
		constraint admins_pkey
			primary key
		constraint id_fk
			references account,
	admin boolean,
	enabled boolean default true not null
);

create table groups_tbl
(
	id bigserial not null
		constraint groups_tbl_pkey
			primary key,
	name varchar(255)
		constraint groups_tbl_groupname_key
			unique,
	description varchar(255),
	creationdate timestamp default CURRENT_TIMESTAMP not null,
	creator_id integer
);

create table grouppic
(
	groupid integer not null
		constraint groups_pics_pkey
			primary key,
	pic bytea
);

create table groupmembershiprequest
(
	account_id integer not null,
	group_id integer not null,
	constraint groups_memb_req_pkey
		primary key (account_id, group_id)
);

create table accountwallmessage
(
	msgid bigserial not null
		constraint wall_msg_pkey
			primary key,
	fromid integer,
	toid integer,
	text text,
	timesent timestamp default CURRENT_TIMESTAMP not null
);

create table accountwallmessagepic
(
	msgid integer not null
		constraint wall_msg_pics_pkey
			primary key
		constraint id_foreign_key
			references accountwallmessage,
	pic bytea
);

create table groupwallmessage
(
	msgid bigserial not null
		constraint groups_wall_msg_pkey
			primary key,
	accid integer,
	groupid integer,
	text text,
	timesent timestamp default CURRENT_TIMESTAMP not null
);

create table groupwallmessagepic
(
	msgid integer not null
		constraint groups_wall_msg_pics_pkey
			primary key
		constraint id_foreign_key
			references groupwallmessage,
	pic bytea
);

create table privatemessage
(
	msgid bigserial not null
		constraint privatemessage_pk
			primary key,
	text varchar,
	fromid bigint not null
		constraint accid2_fk
			references account
				on update cascade,
	toid bigint
		constraint accid1_fk
			references account
				on update cascade,
	timesent timestamp
);

create unique index privatemessage_msgid_uindex
	on privatemessage (msgid);
	
create sequence hibernate_sequence;

